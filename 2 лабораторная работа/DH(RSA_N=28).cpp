﻿// Желамский Егор Алексеевич
// Вариант 7.
// Шифр - Протокол Диффи — Хееллмана.
// N (кол-во знаков) = 28.
// 

#include "iostream"
#include <clocale>
#include <Windows.h>
#include <ctime>
#include <vector>
using namespace std;
vector<int> razp2;
vector<int> combination;
vector<int> combo;

void go(unsigned long long offset, unsigned long long k);

int test_simple(unsigned long long x)
{
	unsigned long long i;
	if (x == 2) {
		return 1;
	}
	if (x == 0 || x == 1 || x % 2 == 0) {
		return 0;
	}
	for (i = 3; i * i <= x && x % i; i += 2);
	return i * i > x;
}

int functbig(unsigned long long down, unsigned long long up, unsigned long long mod) {
	unsigned long long powq = pow(down,up);
	return (powq % mod);
}

int main() {
	srand(static_cast<unsigned int>(time(0)));
	setlocale(LC_ALL, "russian");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	unsigned long long ii = 2, g = 0;
	unsigned long long a =  round(rand() % 20);
	cout << "Генерируем случайное натуральное число a — закрытый ключ: a = " << a << "\n";
	unsigned long long b =  round(rand() % 20);
	while (b == a || (b + 1) == a || (b - 1) == a) {
		b = round(rand() % 20);
	}
	cout << "Генерируем случайное натуральное число b — закрытый ключ: b = " << b << "\n";

	unsigned long long p = abs(round(((rand() % 40))));
	while ( test_simple(p) == 0 || test_simple((p-1)/2) == 0 ) {
		p = abs(round(((rand() % 40))));
	}
	cout << "Открытый параметр p(является случайным простым числом): p = " << p << "\n";

	double p2 = p - 1, buf = 0;
	while (p2 != 1) {
		if (!test_simple(ii)) {
			ii++;
			continue;
		}
		buf = p2 / ii;
		if ((buf-floor(buf)) != 0 ) {
			ii++;
			continue;
		}
		else {
			razp2.push_back(ii);
			ii++;
			p2 = buf;
		}
		if (((p2 / (ii-1)) - floor(p2 / (ii-1))) == 0 ) {
			ii--;
		}
	}
	
	if (razp2.size() == 2 || razp2.size() == 1) {
		go(0, razp2.size()-1);
	}
	if (razp2.size() > 2){
		go(0, 2);
	}
	unsigned long long d = 2, f = 0, s = 0, gg = 0;
	vector<int> oct;
	while (gg != combo.size()) {
		gg = 0;
		if (!test_simple(d)) {
			d++;
			continue;
		}
		for (unsigned long long i = 0; i < combo.size(); i++) {
			//f = pow(d, combo[i]);
			f = d;
			for (int h = 0; h < (combo[i] - 1) ; h++) {
				f = f * d;
			}
			s = f % p;
			oct.push_back(s);
		}
		for (unsigned long long i = 0; i < oct.size(); i++) {
			if (oct[i] == 1) {
				d++;
				oct.clear();
				break;
			}
			else {
				gg +=1;
			}
		}
	}
	g = d;
	cout << "Открытый параметр g(является первообразным корнем по модулю p (также является простым числом)): g = " << g << "\n";
	unsigned long long A = functbig(g, a, p);
	cout << "Открытый ключ A: " << A << "\n";
	cout << "Обмениваемся открытыми ключами с удалённой стороной.\n";
	unsigned long long B = functbig(g, b, p);
	cout << "Открытый ключ B: " << B << "\n";
	unsigned long long K = functbig(A, b, p);
	cout << "Общий секретный ключ K(для Bob): " << K << "\n";
	cout << "Общий секретный ключ K(для Alice): " << functbig(B, a, p) << "\n";
	return 0;
}

void go(unsigned long long offset, unsigned long long k) {
	unsigned long long vce = 1;
	if (k == 0) {
		for (unsigned long long i = 0; i < combination.size(); i++) {
			vce = vce * combination[i];
		}
		combo.push_back(vce);
		return;
	}
	for (unsigned long long i = offset; i <= razp2.size() - k; ++i) {
		combination.push_back(razp2[i]);
		go(i + 1, k - 1);
		combination.pop_back();
	}
}