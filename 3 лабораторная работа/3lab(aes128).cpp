﻿// Желамский Егор Алексеевич
// Современные симметричные шифры. AES-128/AES-256
// 

#include <stdio.h>
#include <stdlib.h>
#include "iostream"
#include <clocale>
#include <fstream>
#include <Windows.h>
#include <ctime>
#include <vector>
using namespace std;
int intext[4][4];
int sbox[16][16];
int invsbox[16][16];
int invMixColumn[4][4];
int rcon[4][10];
int mixColumn[4][4];
int mix_itog[4][4];
int inkey[4][4];
int roundKey[4][4];
int roundKey_buf[4][44];
int masx[4];
int HEX_TO_DEC(char st[10]);
int mix_save(int i, int j);
int inv_mix_save(int i, int j);
int x10_16_sbox(int x, bool choose);
void MixColumns();
void SubBytes();
void ShiftRows();
void AddRoundKey();
void KeyExpansion_full();
void KeyExpansion(int initil);
void InvSubBytes();
void InvShiftRows();
void InvMixColumns();
int mixCol_do(int k, int j, int number);
int invmixCol_do(int k, int j, int number, int date);

int main() {
    setlocale(LC_ALL, "russian");
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    
    ifstream file_text("C:\\3lab_IS\\inputText.txt");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            file_text >> intext[j][i];
        }
    }
    file_text.close();
    
    cout << "Изначальный текст:\n";
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            cout << intext[i][j] << ' ';
        }
        cout << '\n';
    }

    char buf[10];
    ifstream file_sbox("C:\\3lab_IS\\Sbox.txt");
    for (int i = 0; i < 16; i++) {
        for (int j = 0; j < 16; j++) {
            file_sbox >> buf;
            sbox[i][j] = HEX_TO_DEC(buf);
        }
    }
    file_sbox.close();
    ifstream file_invsbox("C:\\3lab_IS\\InvSbox.txt");
    for (int i = 0; i < 16; i++) {
        for (int j = 0; j < 16; j++) {
            file_invsbox >> buf;
            invsbox[i][j] = HEX_TO_DEC(buf);
        }
    }
    file_invsbox.close();
    ifstream file_invmixColumns("C:\\3lab_IS\\InvMixColumns.txt");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            file_invmixColumns >> buf;
            invMixColumn[i][j] = HEX_TO_DEC(buf);
        }
    }
    file_invmixColumns.close();
    ifstream file_rcon("C:\\3lab_IS\\Rcon.txt");
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 4; j++) {
            file_rcon >> buf;
            rcon[j][i] = HEX_TO_DEC(buf);
        }
    }
    file_rcon.close();
    ifstream file_mixColumns("C:\\3lab_IS\\MixColumns.txt");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            file_mixColumns >> buf;
            mixColumn[i][j] = HEX_TO_DEC(buf);
        }
    }
    file_mixColumns.close();

    ifstream file_inkey("C:\\3lab_IS\\inkey.txt");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            file_inkey >> buf;
            inkey[i][j] = HEX_TO_DEC(buf);
            roundKey_buf[i][j] = inkey[i][j];
        }
    }
    file_inkey.close();
    
    KeyExpansion_full();

    KeyExpansion(0);
    AddRoundKey();
    int end = 10;
    for (int h = 1; h < end; h++) {
        SubBytes();
        ShiftRows();
        MixColumns();
        KeyExpansion(h);
        AddRoundKey();
    }
    SubBytes();
    ShiftRows();
    KeyExpansion(end);
    AddRoundKey();
    
    cout << "\nЗашифрованный текст в виде таблицы:\n";
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            cout << intext[i][j] << ' ';
        }
        cout << '\n';
    }
    
    KeyExpansion(end);
    AddRoundKey();
    for (int h = end-1; h > 0; h--) {
        InvShiftRows();
        InvSubBytes();
        KeyExpansion(h);
        AddRoundKey();
        InvMixColumns();
    }
    InvShiftRows();
    InvSubBytes();
    KeyExpansion(0);
    AddRoundKey();
    
    cout << "\nРасшифрованный текст в виде таблицы:\n";
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            cout << intext[i][j] << ' ';
        }
        cout << '\n';
    }

    return 0;
}

void SubBytes() {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            intext[i][j] = x10_16_sbox(intext[i][j], true);
        }
    }
}

void ShiftRows() {
    for (int i = 1; i < 4; i++) {
        for (int j = 0; j < i; j++) {
            int buf = intext[i][0];
            for (int k = 1; k < 4; k++) {
                intext[i][k - 1] = intext[i][k];
            }
            intext[i][3] = buf;
        }
    }
}

void MixColumns() {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            mix_itog[i][j] = mix_save(i, j);
        }
    }
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            intext[i][j] = mix_itog[i][j];
        }
    }
}

int mix_save(int i, int j) {
    int bb = 1, k3_1 = 0, k3_2 = 0;
    for (int k = 0; k < 4; k++) {
        if (mixColumn[i][k] == 1) {
            masx[k] = intext[k][j];
        }
        if (mixColumn[i][k] == 2) {
            masx[k] = mixCol_do(k, j, 2);
        }
        if (mixColumn[i][k] == 3) {
            k3_1 = mixCol_do(k, j, 2);
            masx[k] = k3_1 ^ intext[k][j];
        }
    }
    bb = masx[0];
    for (int k = 1; k < 4; k++) {
        bb ^= masx[k];
    }
    return bb;
}

int mixCol_do(int k, int j, int number) {
    int res = 0;
    res = number * intext[k][j];
    if (intext[k][j] >= 128) {
        res ^= 27;
    }
    if (res > 255) {
        res = res % 256;
    }
    return res;
}

void AddRoundKey() {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            intext[i][j] = intext[i][j] ^ roundKey[i][j];
        }
    }
}

void KeyExpansion_full() {
    for (int i = 4; i < 44; i++) {
        if ((i % 4) == 0) {
            int roundkey_cdvig[4];
            int bufikey = roundKey_buf[0][i - 1];
            for (int g = 1; g < 4; g++) {
                roundkey_cdvig[g - 1] = roundKey_buf[g][i - 1];
            }
            roundkey_cdvig[3] = bufikey;

            for (int g = 0; g < 4; g++) {
                roundkey_cdvig[g] = x10_16_sbox(roundkey_cdvig[g], true);
            }

            for (int j = 0; j < 4; j++) {
                roundKey_buf[j][i] = roundKey_buf[j][i - 4] ^ roundkey_cdvig[j] ^ rcon[j][int(i / (4 - 1))];
            }
        }
        else {
            for (int j = 0; j < 4; j++) {
                roundKey_buf[j][i] = roundKey_buf[j][i - 4] ^ roundKey_buf[j][i - 1];
            }
        }
    }
}

void KeyExpansion(int initil) {
    for (int i = (4 * initil); i < (4 * (initil + 1)); i++) {
        for (int j = 0; j < 4; j++) {
            roundKey[j][i - (4 * initil)] = roundKey_buf[j][i];
        }
    }
}

void InvSubBytes() {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            intext[i][j] = x10_16_sbox(intext[i][j], false);
        }
    }
}

void InvShiftRows() {
    for (int i = 1; i < 4; i++) {
        for (int j = 0; j < i; j++) {
            int buf = intext[i][3];
            for (int k = 3; k > 0; k--) {
                intext[i][k] = intext[i][k - 1];
            }
            intext[i][0] = buf;
        }
    }
}

void InvMixColumns() {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            mix_itog[i][j] = inv_mix_save(i, j);
        }
    }
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            intext[i][j] = mix_itog[i][j];
        }
    }
}

int inv_mix_save(int i,int j) {
    int bb = 1, k3_1 = 0, k3_2 = 0, k3_3 = 0;
    for (int k = 0; k < 4; k++) {
        if (invMixColumn[i][k] == 9) {
            k3_1 = invmixCol_do(k, j, 2, intext[k][j]);
            for (int f = 1; f < 3; f++) {
                k3_1 = invmixCol_do(k,j,2, k3_1);
            }
            masx[k] = k3_1 ^ intext[k][j];
        }
        if (invMixColumn[i][k] == 11) {
            k3_1 = invmixCol_do(k, j, 2, intext[k][j]);
            for (int f = 1; f < 3; f++) {
                k3_1 = invmixCol_do(k, j, 2, k3_1); 
            }
            k3_2 = invmixCol_do(k, j, 2, intext[k][j]);
            masx[k] = k3_1 ^ k3_2 ^ intext[k][j];

        }
        if (invMixColumn[i][k] == 13) {
            k3_1 = invmixCol_do(k, j, 2, intext[k][j]);
            for (int f = 1; f < 3; f++) {
                k3_1 = invmixCol_do(k, j, 2, k3_1);
            }
            k3_2 = invmixCol_do(k, j, 2, intext[k][j]);
            k3_2 = invmixCol_do(k, j, 2, k3_2);
            masx[k] = k3_1 ^ k3_2 ^ intext[k][j];
        }
        if (invMixColumn[i][k] == 14) {
            k3_1 = invmixCol_do(k, j, 2, intext[k][j]);
            for (int f = 1; f < 3; f++) {
                k3_1 = invmixCol_do(k, j, 2, k3_1);
            }
            k3_2 = invmixCol_do(k, j, 2, intext[k][j]);
            k3_2 = invmixCol_do(k, j, 2, k3_2);
            k3_3 = invmixCol_do(k, j, 2, intext[k][j]);
            masx[k] = k3_1 ^ k3_2 ^ k3_3;
        }
    }
    bb = masx[0];
    for (int k = 1; k < 4; k++) {
        bb ^= masx[k];
    }
    return bb;
}

int invmixCol_do(int k, int j, int number, int date) {
    int res = 0;
    if (date < 128) {
        res = number * date;
    }
    else {
        res = number * date;
        res ^= 27;
    }
    if (res > 255) {
        res = res % 256;
    }

    return res;
}

int x10_16_sbox(int x, bool choose) {
    int buf1, buf2;
    buf2 = x % 16;
    buf1 = (x - buf2) / 16;
    if (choose) {
        return sbox[buf1][buf2];
    }
    else {
        return invsbox[buf1][buf2];
    }

}

int HEX_TO_DEC(char st[10])
{
    int i, s, k, p;
    s = 0;
    p = strlen(st) - 1;
    for (i = 0; st[i] != '\0'; i++) {
        switch (toupper(st[i])) {
        case 'A':
            k = 10;
            break;
        case 'B':
            k = 11;
            break;
        case 'C':
            k = 12;
            break;
        case 'D':
            k = 13;
            break;
        case 'E':
            k = 14;
            break;
        case 'F':
            k = 15;
            break;
        case '1':
            k = 1;
            break;
        case '2':
            k = 2;
            break;
        case '3':
            k = 3;
            break;
        case '4':
            k = 4;
            break;
        case '5':
            k = 5;
            break;
        case '6':
            k = 6;
            break;
        case '7':
            k = 7;
            break;
        case '8':
            k = 8;
            break;
        case '9':
            k = 9;
            break;
        case '0':
            k = 0;
            break;
        }
        s = s + k * pow(16, p);
        p--;
    }
    return s;
}