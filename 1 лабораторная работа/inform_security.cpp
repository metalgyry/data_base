﻿//Желамский Егор Алексеевич
//Вариант 7: Задание - Шифр Трисемуса
//

#include <iostream>
#include <clocale>
#include <Windows.h>
using namespace std;

int main() {
	setlocale(LC_ALL, "russian");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int pos = 0, sizekey = 0;
	bool two = false;
	char alphabet[34] = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
	char key[255] = { }, keyit[255] = { };
	///cin.getline;
	cout << "Введите ключевое слово:\n";
	gets_s(key,255);
	// сокращение слова
	for (int i = 0; key[i] != NULL; i++) {
		char buf = key[i];
		for (int j = 0; j < i;j++) {
			if (buf == key[j]) {
				two = true;
				break;
			}
			else {
				two = false;
			}
		}
		if (!two) {
			keyit[pos] = buf;
			pos++;
			two = false;
		}
	}
	// вывод ключа
	cout << "\n" << "Ключ: " ;
	for (int i = 0;keyit[i] != NULL;i++) {
		cout << keyit[i];
		sizekey++;
	}
	cout << "\n\n";
	// получение полной строки которая будет заполнять таблицу (//дешифратор)
	for (int i = 0; alphabet[i] != NULL; i++) {
		char buf = alphabet[i];
		for (int j = 0;j < sizekey; j++) {
			if (buf == keyit[j]) {
				two = true;
				break;
			}
			else {
				two = false;
			}
		}
		if (!two) {
			keyit[pos] = buf;
			pos++;
			two = false;
		}
	}
	// вывод строки (//дешифратор)
	cout << "\n" << "Строка которая будет вводится в таблицу: ";
	for (int i = 0; keyit[i] != NULL; i++) {
		cout << keyit[i];
	}
	cout << "\n\n";
	// ввод размеров таблицы
	int x, y;
	do {
		cout << "Введите размер таблицы(если вы это видите снова, значит размер таблицы задан неверно):\nX(колич. столбцов) = ";
		cin >> x;
		cout << "\nY(колич. строк) = ";
		cin >> y;
	} while ((x*y < 33)); // (x*y < 33) || (x*y > 64)
	// вывод таблицы  (//дешифратор)
	cout << "\n\nТаблица:\n";
	int l = 0;
	for (int i = 0; i < y; i++) {
		for (int j = 0; j < x; j++) {
			cout << keyit[l] << " ";
			l++;
		}
		cout << "\n";
		if (keyit[l] == NULL) {
			break;
		}
	}
	// ввод слова для закодирования
	char str[255] = { }, itog[255] = { };
	cout << "Введите слово которое хотите закодировать:\n";
	cin.ignore();
	gets_s(str);
	// шифрование слова
	for (int i = 0; str[i] != NULL; i++) {
		char buf = str[i];
		for (int j = 0; keyit[j] != NULL; j++) {
			if (buf == keyit[j]) {                   // узнать про пустые ячейки
				itog[i] = keyit[((j+x)%33)];
				break;
			}
		}
	}
	// вывод слова зашифрованного
	cout << "\nЗашифрованное слово:\n";
	for(int i = 0; itog[i] != NULL ; i++) {
		cout << itog[i];
	}
	// дешифратор
	char def[255] = { };
	for (int i = 0; itog[i] != NULL; i++) {
		char buf = itog[i];
		for (int j = 0; keyit[j] != NULL; j++) {
			if (buf == keyit[j]) {
				if ((j-x) < 0) {
					def[i] = keyit[33+(j-x)];
					break;
				}
				else {
					def[i] = keyit[(j - x)];
				}


				def[i] = keyit[((j - x) % 33)];
				break;
			}
		}
	}
	cout << "\n\nСлово которое было зашифрованно:\n";
	for (int i = 0; def[i] != NULL; i++) {
		cout << def[i];
	}
	cout << "\n\n\n";


	return 0;
}